package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Resultado extends JFrame {

	private JPanel contentPane;
	private JLabel lblMedicamento;
	private JLabel lblDireccion;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Resultado frame = new Resultado();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	
	

	/**
	 * Create the frame.
	 */
	public Resultado() {
		setTitle("Pedido al ");
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 535, 268);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		setContentPane(contentPane);
				
		
		JLabel lblNombreDelMedicamento = new JLabel("Medicamento Solicitado");
		lblNombreDelMedicamento.setFont(new Font("Arial", Font.PLAIN, 23));
		
		lblMedicamento = new JLabel("\" \"");
		
		
		
		
		JLabel lblDireccinDeEnvi = new JLabel("Direcci\u00F3n de envi\u00F3");
		lblDireccinDeEnvi.setFont(new Font("Arial", Font.PLAIN, 23));
		
		lblDireccion = new JLabel("\" \"");
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Pedido Enviado");
				dispose();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(10)
							.addComponent(lblMedicamento))
						.addComponent(lblNombreDelMedicamento)
						.addComponent(lblDireccinDeEnvi)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(10)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(btnCancelar)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(btnEnviar))
								.addComponent(lblDireccion))))
					.addContainerGap(140, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(lblNombreDelMedicamento)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblMedicamento)
					.addGap(31)
					.addComponent(lblDireccinDeEnvi)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblDireccion)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCancelar)
						.addComponent(btnEnviar))
					.addContainerGap(24, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
	
	public void setText (String texto, String texto1) {
		if (! texto.isEmpty() && ! texto1.isEmpty()) {
			lblMedicamento.setText(texto);
			lblDireccion.setText(texto1);
		}
	}
	

	public JLabel getLblMedicamento() {
		return lblMedicamento;
	}
	public JLabel getLblDireccion() {
		return lblDireccion;
	}
}
