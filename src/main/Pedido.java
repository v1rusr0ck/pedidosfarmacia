package main;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.eclipse.swt.events.KeyAdapter;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import main.Resultado;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.function.Function;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class Pedido {

	private JFrame frame;
	private JTextField txtMedicamento;
	private JTextField txtCantidad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pedido window = new Pedido();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pedido() {
		initialize();		
	}
	
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();		
		frame.setBounds(100, 100, 507, 339);
		frame.setLocationRelativeTo(null);
		frame.setTitle("Simulador de Pedidos Farmaceuticos");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblNewLabel = new JLabel("Realizar nuevo pedido al proveedor");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 20));
		
		JLabel lblNewLabel_1 = new JLabel("Nombre del medicamento");
		
		txtMedicamento = new JTextField();
		txtMedicamento.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (txtMedicamento.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "El nombre del medicamento es obligatorio", "WARNING", JOptionPane.WARNING_MESSAGE);
					txtMedicamento.requestFocus();
				}
			}
		});
		txtMedicamento.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Tipo del medicamento");
		
		JComboBox<String> cmbTipoMedicamento = new JComboBox();
		cmbTipoMedicamento.setModel(new DefaultComboBoxModel(new String[] {"Analg\u00E9sico", "Anal\u00E9ptico", "Anest\u00E9sico", "Anti\u00E1cido", "Antidepresivo", "Antibi\u00F3ticos"}));
				
		
		JLabel lblCantidadRequerida = new JLabel("Cantidad Requerida");
		
		txtCantidad = new JTextField(new Integer(3));
		txtCantidad.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtCantidad.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Debe de ingresar una cantidad", "Warning", JOptionPane.WARNING_MESSAGE);
					txtCantidad.requestFocus();
				}
			}
		});
		txtCantidad.setColumns(10);
		//Agregamos un listener al teclado para capturar tecla presionad 
		//Para validar que solo pueda ingresar n�meros positivos
		txtCantidad.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				char caracter = e.getKeyChar();
				if ((caracter < '0')||
					(caracter > '9') &&
					(caracter != '\b')) 
				{
					e.consume();
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		JOptionPane ventana = new JOptionPane();
		
		JLabel lblDistribuidor = new JLabel("Distribuidor");
		
		JRadioButton rbtnCofarma = new JRadioButton("Cofarma");		
		JRadioButton rbtnEmpsepha = new JRadioButton("Empsepha");		
		JRadioButton rbtnCemefar = new JRadioButton("Cemefar");
		
		ButtonGroup distribuidor = new ButtonGroup();
		distribuidor.add(rbtnCemefar);
		distribuidor.add(rbtnCofarma);
		distribuidor.add(rbtnEmpsepha);
		
		
		
		JLabel lblSucursalReceptora = new JLabel("Sucursal receptora");
		
		JCheckBox chckbxFarmaciaPrincipal = new JCheckBox("Farmacia principal");
		
		JCheckBox chckbxFarmaciaSecundaria = new JCheckBox("Farmacia Secundaria");
		
		
		JDialog emergente = new JDialog();
		
		JButton btnPedido = new JButton("Realizar pedido");
		
		btnPedido.addActionListener(new ActionListener() {
			private String cantidad;
			private String nombreMedicamento;
			private String texto;
			private String texto1;

			public void actionPerformed(ActionEvent arg0) {
				
					nombreMedicamento = txtMedicamento.getText();
					cantidad = txtCantidad.getText();
							
				Object tipoMedicamento = cmbTipoMedicamento.getSelectedItem();
				String nomDistribuidor;
				if (rbtnCofarma.isSelected() == true) {
					nomDistribuidor = rbtnCofarma.getLabel();
				}
				else if (rbtnEmpsepha.isSelected() == true) {
						nomDistribuidor = rbtnEmpsepha.getLabel();				
				} else {
					nomDistribuidor = rbtnCemefar.getLabel();
				}
				
				if (chckbxFarmaciaPrincipal.isSelected() && chckbxFarmaciaSecundaria.isSelected()) {
					texto1 = "Para la farmacia situada en Calle de la Rosa n.28 y para la situada en Calle Alcazabilla n.3";
				}else if (chckbxFarmaciaPrincipal.isSelected()) {
					texto1 = "Para la farmacia situada en Calle de la Rosa n.28";
				}else if (chckbxFarmaciaSecundaria.isSelected()) {
					texto1 = "Para la farmacia situada en Calle Alcazabilla n.3";
				}
				
				texto = cantidad + " unidades del " + tipoMedicamento.toString() +" " + nombreMedicamento ;
				
				Resultado resultado = new Resultado();
				resultado.setVisible(true);
				resultado.setTitle("Pedido al distribuidor " + nomDistribuidor);
				resultado.setText(texto, texto1);
				
				
			}
		});
		
		
		
		JButton btnBorrarFormulario = new JButton("Borrar Formulario");
		btnBorrarFormulario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtCantidad.setText(null);
				txtMedicamento.setText(null);
				cmbTipoMedicamento.setSelectedIndex(0);
				distribuidor.clearSelection();				
				chckbxFarmaciaPrincipal.setSelected(false);
				chckbxFarmaciaSecundaria.setSelected(false);
				
			}
		});
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(48)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblSucursalReceptora)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(chckbxFarmaciaPrincipal)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(chckbxFarmaciaSecundaria))
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 332, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel_1)
										.addComponent(lblNewLabel_2)
										.addComponent(lblCantidadRequerida)
										.addComponent(lblDistribuidor))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(rbtnCofarma)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(rbtnEmpsepha)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(rbtnCemefar))
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
											.addComponent(txtCantidad)
											.addComponent(cmbTipoMedicamento, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(txtMedicamento, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE))))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(74)
							.addComponent(btnBorrarFormulario)
							.addGap(29)
							.addComponent(btnPedido)))
					.addContainerGap(108, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(27)
					.addComponent(lblNewLabel)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(txtMedicamento, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_2)
						.addComponent(cmbTipoMedicamento, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCantidadRequerida)
						.addComponent(txtCantidad, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDistribuidor)
						.addComponent(rbtnCofarma)
						.addComponent(rbtnEmpsepha)
						.addComponent(rbtnCemefar))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(chckbxFarmaciaSecundaria)
						.addComponent(chckbxFarmaciaPrincipal)
						.addComponent(lblSucursalReceptora))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnBorrarFormulario)
						.addComponent(btnPedido))
					.addContainerGap(42, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
